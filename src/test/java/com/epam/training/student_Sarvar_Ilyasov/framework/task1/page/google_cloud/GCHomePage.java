package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.google_cloud;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.AbstractPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GCHomePage extends AbstractPage {
    private final String HOMEPAGE_URL = "https://cloud.google.com/";
    @FindBy(className = "YSM5S")
    private WebElement searchIcon;
    @FindBy(name = "q")
    private WebElement searchTextBox;
    public GCHomePage(WebDriver driver) {
        super(driver);
    }

    public GCHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        logger.info("Opened Google Cloud Home page");
        return this;
    }

    public GCSearchResultsPage searchForTerms(String term) {
        searchIcon.click();
        searchTextBox.sendKeys(term);
        searchTextBox.sendKeys(Keys.ENTER);
        logger.info("Searched for [" + term + "]");
        return new GCSearchResultsPage(driver);
    }
}

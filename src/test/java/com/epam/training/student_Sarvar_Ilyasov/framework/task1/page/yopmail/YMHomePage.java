package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.yopmail;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YMHomePage extends AbstractPage {
    private final String HOMEPAGE_URL = "https://yopmail.com/";
    private final String RANDOM_EMAIL_URL = "https://yopmail.com/email-generator";
    @FindBy(className = "txtlien")
    private WebElement randomEmailGeneratorButton;

    public YMHomePage(WebDriver driver) {
        super(driver);
    }

    public YMHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        logger.info("Opened YOPMail Home page.");
        return this;
    }

    public YMEmailGeneratorPage createRandomEmail() {
        randomEmailGeneratorButton.click();
        driver.get(RANDOM_EMAIL_URL);
        logger.info("Created random email.");
        return new YMEmailGeneratorPage(driver);
    }
}

package com.epam.training.student_Sarvar_Ilyasov.framework.task1.test;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.util.StringUtils;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.util.TabUtils;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.yopmail.YMEmailInboxPage;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.yopmail.YMHomePage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class GoogleCloudTest extends CommonConditions {
    @Test
    public void testEstimatedCostVisible() {
        Assertions.assertTrue(googleCloudTab.getTotalEstimatedCost().contains("Total Estimated Cost: USD"),
                "Estimated Cost in GC Pricing Calculator is not visible.");
    }

    @Test
    public void testEstimatedCostInInbox() {
        String totalEstimatedCost = StringUtils.extractCost(googleCloudTab.getTotalEstimatedCost());
        googleCloudTab.emailEstimate();
        TabUtils.newTab(driver);
        YMEmailInboxPage yopmailTab = new YMHomePage(driver).openPage()
                .createRandomEmail().copyEmail().checkEmailInbox();
        TabUtils.switchToGoogle(driver);
        googleCloudTab.updateDriver(driver).fillEmail().sendEmail();
        TabUtils.switchToYopmail(driver);
        Assertions.assertTrue(yopmailTab.updateDriver(driver)
                        .getEstimatedCost(totalEstimatedCost).contains(totalEstimatedCost),
                "Estimated Cost which is sent by GC Pricing Calculator does not match.");
    }
}

package com.epam.training.student_Sarvar_Ilyasov.framework.task1.model;

import java.util.Objects;

public class Calculator {
    private String numberOfInstances;
    private String addGPU;

    public Calculator(String numberOfInstances, String addGPU) {
        this.numberOfInstances = numberOfInstances;
        this.addGPU = addGPU;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public String getAddGPU() {
        return addGPU;
    }

    public void setAddGPU(String addGPU) {
        this.addGPU = addGPU;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Calculator that = (Calculator) o;
        return Objects.equals(numberOfInstances, that.numberOfInstances)
                && Objects.equals(addGPU, that.addGPU);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfInstances, addGPU);
    }
}
package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.google_cloud;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GCSearchResultsPage extends AbstractPage {
    @FindBy(xpath = "//b[contains(text(),'Google Cloud Pricing Calculator')]")
    private WebElement calculatorLink;
    public GCSearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public GCPricingCalculatorPage openCalculator() {
        calculatorLink.click();
        logger.info("Opened the Calculator.");
        return new GCPricingCalculatorPage(driver);
    }
}

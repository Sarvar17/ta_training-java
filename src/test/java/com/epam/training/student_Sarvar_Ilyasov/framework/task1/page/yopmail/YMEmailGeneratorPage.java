package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.yopmail;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class YMEmailGeneratorPage extends AbstractPage {
    @FindBy(id = "cprnd")
    private WebElement copyEmailButton;
    @FindBy(xpath = "//span[contains(text(),'Check Inbox')]")
    private WebElement checkEmailInboxButton;
    public YMEmailGeneratorPage(WebDriver driver) {
        super(driver);
    }

    public YMEmailGeneratorPage copyEmail() {
        wait.until(ExpectedConditions.visibilityOf(copyEmailButton)).click();
        logger.info("Copied email");
        return this;
    }

    public YMEmailInboxPage checkEmailInbox() {
        wait.until(ExpectedConditions.visibilityOf(checkEmailInboxButton)).click();
        return new YMEmailInboxPage(driver);
    }
}

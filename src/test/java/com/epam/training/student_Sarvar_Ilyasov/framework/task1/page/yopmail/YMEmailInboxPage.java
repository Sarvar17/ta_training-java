package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.yopmail;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class YMEmailInboxPage extends AbstractPage {
    @FindBy(id = "refresh")
    private WebElement refreshButton;
    @FindBy(id = "mail")
    private WebElement estimatedCost;
    public YMEmailInboxPage(WebDriver driver) {
        super(driver);
    }

    public String getEstimatedCost(String totalEstimatedCost) {
        refreshButton.click();
        logger.info("Started to wait the mail with the Total estimated cost");
        try {
            driver.switchTo().frame("ifmail");
            wait.until(ExpectedConditions.visibilityOf(estimatedCost));
        } catch (Exception e) {
            driver.switchTo().defaultContent();
            return getEstimatedCost(totalEstimatedCost);
        }
        logger.info("Appeared the Total estimated cost");
        return estimatedCost.getText();
    }

    public YMEmailInboxPage updateDriver(WebDriver driver) {
        setDriver(driver);
        return this;
    }
}

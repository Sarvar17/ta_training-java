package com.epam.training.student_Sarvar_Ilyasov.framework.task1.service;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.model.Calculator;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.google_cloud.GCPricingCalculatorPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CalculatorService {
    public static final String TESTDATA_NUMBER_OF_INSTANCES = "testdata.calculator.instances";
    public static final String TESTDATA_ADD_GPU = "testdata.calculator.gpu";
    public static final Logger logger = LogManager.getRootLogger();

    public static Calculator withDefaultParamsFromProperty(){
        return new Calculator(TestDataReader.getTestData(TESTDATA_NUMBER_OF_INSTANCES),
                TestDataReader.getTestData(TESTDATA_ADD_GPU));
    }

    public static GCPricingCalculatorPage fillForm(GCPricingCalculatorPage page, Calculator calculator) {
        logger.info("Started filling the Form.");
        page.enterNumberOfInstances(calculator.getNumberOfInstances())
                .selectOperatingSystem(page.freeOperatingSystem)
                .selectProvisioningModel(page.regularModel)
                .selectMachineFamily(page.generalFamily)
                .selectSeries(page.n1Series)
                .selectMachineType(page.machineTypeOption);
        if (calculator.getAddGPU().equals("true")) {
            page.clickAddGPUCheckBox()
                    .selectGPUType(page.nvidiaTesla)
                    .selectNumberOfGPU(page.numberOfGPUOption);
        }
        page.selectLocalSSD(page.localSSDOption)
                .selectDataCenter(page.frankfurtDataCenter)
                .selectCommittedUsage(page.committedUsageOption);
        logger.info("Filled the form.");
        return page;
    }
}

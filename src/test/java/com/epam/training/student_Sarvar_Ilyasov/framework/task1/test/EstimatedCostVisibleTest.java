package com.epam.training.student_Sarvar_Ilyasov.framework.task1.test;

import org.testng.Assert;
import org.testng.annotations.Test;

public class EstimatedCostVisibleTest extends CommonConditions {
    @Test
    public void testEstimatedCostVisible() {
        Assert.assertTrue(googleCloudTab.getTotalEstimatedCost().contains("Total Estimated Cost: USD"),
                "Estimated Cost in GC Pricing Calculator is not visible.");
    }
}

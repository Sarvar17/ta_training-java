package com.epam.training.student_Sarvar_Ilyasov.framework.task1.util;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import java.util.ArrayList;
import java.util.List;

public class TabUtils {
    public static List<String> tabs;
    public static void newTab(WebDriver driver) {
        driver.switchTo().newWindow(WindowType.TAB);
        driver.manage().window().setSize(new Dimension(1000, 1000));
        tabs = new ArrayList<>(driver.getWindowHandles());
    }

    public static void switchToGoogle(WebDriver driver) {
        driver.switchTo().window(tabs.get(0));
        driver.manage().window().maximize();
    }

    public static void switchToYopmail(WebDriver driver) {
        driver.switchTo().window(tabs.get(1));
        driver.manage().window().setSize(new Dimension(1000, 1000));
    }
}

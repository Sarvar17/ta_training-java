package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.google_cloud;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.AbstractPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GCPricingCalculatorPage extends AbstractPage {
    @FindBy(xpath = "//span[text()='Compute Engine']")
    private WebElement computeEngineTab;
    @FindBy(xpath = "//label[contains(text(), 'Number of instances')]/following-sibling::input")
    private WebElement numberOfInstancesInput;
    @FindBy(xpath = "//label[text()='Operating System / Software']/following-sibling::md-select")
    private WebElement operatingSystemSelect;
    @FindBy(xpath = "//md-option[contains(., 'Free: Debian')]")
    public WebElement freeOperatingSystem;
    @FindBy(xpath = "//label[text()='Provisioning model']/following-sibling::md-select")
    private WebElement provisioningModelSelect;
    @FindBy(xpath = "//md-option[contains(., 'Regular')]")
    public WebElement regularModel;
    @FindBy(xpath = "//label[text()='Machine Family']/following-sibling::md-select")
    private WebElement machineFamilySelect;
    @FindBy(xpath = "//md-option[contains(., 'General')]")
    public WebElement generalFamily;
    @FindBy(xpath = "//label[text()='Series']/following-sibling::md-select")
    private WebElement seriesSelect;
    @FindBy(xpath = "//md-option[contains(., 'N1')]")
    public WebElement n1Series;
    @FindBy(xpath = "//label[text()='Machine type']/following-sibling::md-select")
    private WebElement machineTypeSelect;
    @FindBy(xpath = "//md-option[contains(., 'n1-standard-8')]")
    public WebElement machineTypeOption;
    @FindBy(xpath = "//md-checkbox[contains(.,'Add GPUs.')]")
    private WebElement addGPUCheckBox;
    @FindBy(xpath = "//label[text()='GPU type']/following-sibling::md-select")
    private WebElement gpuTypeSelect;
    @FindBy(xpath = "//md-option[contains(., 'NVIDIA Tesla V100')]")
    public WebElement nvidiaTesla;
    @FindBy(xpath = "//label[text()='Number of GPUs']/following-sibling::md-select")
    private WebElement numberOfGPUSelect;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[@value='1']")
    public WebElement numberOfGPUOption;
    @FindBy(xpath = "//label[text()='Local SSD']/following-sibling::md-select")
    private WebElement localSSDSelect;
    @FindBy(xpath = "//md-option[contains(., '2x375')]")
    public WebElement localSSDOption;
    @FindBy(xpath = "//label[text()='Datacenter location']/following-sibling::md-select")
    private WebElement dataCenterSelect;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[contains(., 'Frankfurt')]")
    public WebElement frankfurtDataCenter;
    @FindBy(xpath = "//label[text()='Committed usage']/following-sibling::md-select")
    private WebElement committedUsageSelect;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[contains(., '1 Year')]")
    public WebElement committedUsageOption;
    @FindBy(xpath = "//button[contains(text(),'Add to Estimate')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//b[contains(text(), 'Total Estimated Cost:')]")
    private WebElement estimatedCost;
    @FindBy(id = "Email Estimate")
    private WebElement emailEstimateButton;
    @FindBy(xpath = "//label[text()='Email ']/following-sibling::input")
    private WebElement emailInput;
    @FindBy(xpath = "//button[contains(text(),'Send Email')]")
    private WebElement sendEmailButton;
    public GCPricingCalculatorPage(WebDriver driver) {
        super(driver);
        if (!driver.getCurrentUrl().contains("calculator-legacy")) {
            driver.get("https://cloud.google.com/products/calculator-legacy");
        }
        setFrame();
    }

    public GCPricingCalculatorPage updateDriver(WebDriver driver) {
        setDriver(driver);
        setFrame();
        return this;
    }

    private void setFrame() {
        this.driver.switchTo().frame(0);
        this.driver.switchTo().frame(0);
    }

    public GCPricingCalculatorPage chooseComputeEngine() {
        computeEngineTab.click();
        return this;
    }

    public GCPricingCalculatorPage selectDataCenter(WebElement dataCenterOption) {
        dataCenterSelect.click();
        waitVisibility(dataCenterOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectLocalSSD(WebElement localSSDOption) {
        localSSDSelect.click();
        waitVisibility(localSSDOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectNumberOfGPU(WebElement numberOfGPUOption) {
        numberOfGPUSelect.click();
        waitVisibility(numberOfGPUOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectGPUType(WebElement gpuTypeOption) {
        gpuTypeSelect.click();
        waitVisibility(gpuTypeOption).click();
        return this;
    }

    public GCPricingCalculatorPage clickAddGPUCheckBox() {
        addGPUCheckBox.click();
        return this;
    }

    public GCPricingCalculatorPage selectMachineType(WebElement machineTypeOption) {
        machineTypeSelect.click();
        waitVisibility(machineTypeOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectSeries(WebElement seriesOption) {
        seriesSelect.click();
        waitVisibility(seriesOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectCommittedUsage(WebElement committedUsageOption) {
        committedUsageSelect.click();
        waitVisibility(committedUsageOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectMachineFamily(WebElement machineFamilyOption) {
        machineFamilySelect.click();
        waitVisibility(machineFamilyOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectProvisioningModel(WebElement provisioningModelOption) {
        provisioningModelSelect.click();
        waitVisibility(provisioningModelOption).click();
        return this;
    }

    public GCPricingCalculatorPage selectOperatingSystem(WebElement operatingSystemOption) {
        operatingSystemSelect.click();
        waitVisibility(operatingSystemOption).click();
        return this;
    }

    public GCPricingCalculatorPage enterNumberOfInstances(String number) {
        numberOfInstancesInput.sendKeys(number);
        return this;
    }

    public GCPricingCalculatorPage addToEstimate() {
        waitVisibility(addToEstimateButton).click();
        return this;
    }

    public GCPricingCalculatorPage emailEstimate() {
        waitToBeClickable(emailEstimateButton).click();
        return this;
    }

    public GCPricingCalculatorPage fillEmail() {
        emailInput.sendKeys(Keys.CONTROL + "v");
        return this;
    }

    public GCPricingCalculatorPage sendEmail() {
        waitToBeClickable(sendEmailButton).click();
        return this;
    }

    public String getTotalEstimatedCost() {
        return waitVisibility(estimatedCost).getText();
    }

    public WebElement waitVisibility(WebElement webElement) {
        return wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public WebElement waitToBeClickable(WebElement webElement) {
        return wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }
}

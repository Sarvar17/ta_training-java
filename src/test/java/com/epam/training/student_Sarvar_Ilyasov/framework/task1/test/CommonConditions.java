package com.epam.training.student_Sarvar_Ilyasov.framework.task1.test;

import com.epam.training.student_Sarvar_Ilyasov.framework.task1.driver.DriverSingleton;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.google_cloud.GCHomePage;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.page.google_cloud.GCPricingCalculatorPage;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.service.CalculatorService;
import com.epam.training.student_Sarvar_Ilyasov.framework.task1.util.TestListener;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public abstract class CommonConditions {
    protected static WebDriver driver;
    protected static GCPricingCalculatorPage googleCloudTab;

    @BeforeAll()
    public static void setUp() {
        driver = DriverSingleton.getDriver();
        googleCloudTab = new GCHomePage(driver)
                .openPage()
                .searchForTerms("Google Cloud Platform Pricing Calculator")
                .openCalculator()
                .chooseComputeEngine();

        googleCloudTab = CalculatorService
                .fillForm(googleCloudTab, CalculatorService.withDefaultParamsFromProperty())
                .addToEstimate();
    }

    @AfterAll
    public static void tearDown() {
        DriverSingleton.closeDriver();
    }
}

package com.epam.training.student_Sarvar_Ilyasov.framework.task1.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class AbstractPage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected final Logger logger = LogManager.getRootLogger();
    public AbstractPage(WebDriver driver) {
        setUp(driver);
        PageFactory.initElements(this.driver, this);
    }

    public void setDriver(WebDriver driver) {
        setUp(driver);
    }

    private void setUp(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }
}

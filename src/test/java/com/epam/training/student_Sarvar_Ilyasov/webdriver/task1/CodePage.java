package com.epam.training.student_Sarvar_Ilyasov.webdriver.task1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CodePage {
    @FindBy(className = "source")
    private WebElement codeElement;
    @FindBy(className = "info-top")
    private WebElement pasteName;
    private final WebDriver driver;

    public CodePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public String getCode() {
        return codeElement.getText();
    }

    public String getPasteName() {
        return pasteName.getText();
    }
}

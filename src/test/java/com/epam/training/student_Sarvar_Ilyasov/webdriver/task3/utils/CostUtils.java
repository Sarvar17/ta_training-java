package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CostUtils {
    public static String extractCost(String totalEstimatedCost) {
        Pattern pattern = Pattern.compile("USD [0-9,\\.]+");
        Matcher matcher = pattern.matcher(totalEstimatedCost);
        if (matcher.find()) {
            return matcher.group();
        }
        throw new IllegalArgumentException("Unable to extract cost from the provided string: "
                + totalEstimatedCost);
    }
}
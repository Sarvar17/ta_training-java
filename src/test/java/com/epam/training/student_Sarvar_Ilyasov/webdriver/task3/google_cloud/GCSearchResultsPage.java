package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.google_cloud;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GCSearchResultsPage {
    private final WebDriver driver;
    @FindBy(xpath = "//b[contains(text(),'Google Cloud Pricing Calculator')]")
    private WebElement calculatorLink;
    public GCSearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public GCPricingCalculatorPage openCalculator() {
        calculatorLink.click();
        return new GCPricingCalculatorPage(driver);
    }
}

package com.epam.training.student_Sarvar_Ilyasov.webdriver.task1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PastebinTest {
    private static WebDriver driver;
    private static CodePage page;

    @BeforeAll
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        page = new HomePage(driver).openPage()
                .enterCode("Hello from WebDriver").selectExpirationTenMinutes()
                .enterPasteName("helloweb").clickCreateButton();
    }

    @Test
    public void checkTitle() {
        Assertions.assertEquals("helloweb", page.getPasteName(),
                "Page title does not match");
    }

    @Test
    public void checkCode() {
        Assertions.assertEquals("Hello from WebDriver", page.getCode(),
                "Code does not match");
    }

    @AfterAll
    public static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}

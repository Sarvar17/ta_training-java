package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.yopmail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class YMEmailInboxPage {
    private WebDriver driver;
    private WebDriverWait wait;
    @FindBy(id = "refresh")
    private WebElement refreshButton;
    @FindBy(id = "mail")
    private WebElement estimatedCost;
    public YMEmailInboxPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public String getEstimatedCost(String totalEstimatedCost) {
        refreshButton.click();
        try {
            driver.switchTo().frame("ifmail");
            wait.until(ExpectedConditions.visibilityOf(estimatedCost));
        } catch (Exception e) {
            driver.switchTo().defaultContent();
            return getEstimatedCost(totalEstimatedCost);
        }
        return estimatedCost.getText();
    }

    public YMEmailInboxPage updateDriver(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        return this;
    }
}

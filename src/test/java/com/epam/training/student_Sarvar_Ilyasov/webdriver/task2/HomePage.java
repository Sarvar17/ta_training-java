package com.epam.training.student_Sarvar_Ilyasov.webdriver.task2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    @FindBy(id = "postform-text")
    private WebElement codeTextArea;
    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expirationDropdown;
    @FindBy(xpath = "//li[text()='10 Minutes']")
    private WebElement expirationDropdownOption;
    @FindBy(id = "postform-name")
    private WebElement pasteNameField;
    @FindBy(xpath = "//button[text()='Create New Paste']")
    private WebElement createButton;
    @FindBy(id = "select2-postform-format-container")
    private WebElement syntaxDropdown;
    @FindBy(xpath = "//li[text()='Bash']")
    private WebElement syntaxDropdownOption;
    private final WebDriver driver;
    private final String HOMEPAGE_URL = "https://pastebin.com/";

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public HomePage openPage() {
        driver.get(HOMEPAGE_URL);
        return this;
    }

    public HomePage enterCode(String code) {
        codeTextArea.sendKeys(code);
        return this;
    }

    public HomePage selectExpirationTenMinutes() {
        expirationDropdown.click();
        expirationDropdownOption.click();
        return this;
    }

    public HomePage enterPasteName(String pasteName) {
        pasteNameField.sendKeys(pasteName);
        return this;
    }

    public HomePage selectBashSyntaxHighlighting() {
        syntaxDropdown.click();
        syntaxDropdownOption.click();
        return this;
    }

    public CodePage clickCreateButton() {
        createButton.click();
        return new CodePage(driver);
    }
}
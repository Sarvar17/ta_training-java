package com.epam.training.student_Sarvar_Ilyasov.webdriver.task2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PatebinTest {
    private static WebDriver driver;
    private static CodePage page;
    private static final String code = """
            git config --global user.name  "New Sheriff in Town"
            git reset $(git commit-tree HEAD^{tree} -m "Legacy code")
            git push origin master --force""";

    @BeforeAll
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        page = (new HomePage(driver)).openPage().enterCode(code)
                .selectBashSyntaxHighlighting().selectExpirationTenMinutes()
                .enterPasteName("how to gain dominance among developers")
                .clickCreateButton();
    }

    @Test
    public void checkTitle() {
        Assertions.assertEquals("how to gain dominance among developers",
                page.getPasteName(), "Page title does not match");
    }

    @Test
    public void checkSyntax() {
        Assertions.assertEquals("Bash", page.getSyntaxHighlighting(),
                "Page syntax does not match");
    }

    @Test
    public void checkCode() {
        Assertions.assertEquals(code, page.getCode(),
                "Code does not match");
    }

    @AfterAll
    public static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}

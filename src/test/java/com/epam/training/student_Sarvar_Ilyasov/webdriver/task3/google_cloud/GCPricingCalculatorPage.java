package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.google_cloud;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class GCPricingCalculatorPage {
    private WebDriver driver;
    private WebDriverWait wait;
    @FindBy(xpath = "//span[text()='Compute Engine']")
    private WebElement computeEngineTab;
    @FindBy(xpath = "//label[contains(text(), 'Number of instances')]/following-sibling::input")
    private WebElement numberOfInstancesInput;
    @FindBy(xpath = "//label[text()='Operating System / Software']/following-sibling::md-select")
    private WebElement operatingSystemSelect;
    @FindBy(xpath = "//md-option[contains(., 'Free: Debian')]")
    private WebElement freeOperatingSystem;
    @FindBy(xpath = "//label[text()='Provisioning model']/following-sibling::md-select")
    private WebElement provisioningModelSelect;
    @FindBy(xpath = "//md-option[contains(., 'Regular')]")
    private WebElement regularModel;
    @FindBy(xpath = "//label[text()='Machine Family']/following-sibling::md-select")
    private WebElement machineFamilySelect;
    @FindBy(xpath = "//md-option[contains(., 'General')]")
    private WebElement generalFamily;
    @FindBy(xpath = "//label[text()='Series']/following-sibling::md-select")
    private WebElement seriesSelect;
    @FindBy(xpath = "//md-option[contains(., 'N1')]")
    private WebElement n1Series;
    @FindBy(xpath = "//label[text()='Machine type']/following-sibling::md-select")
    private WebElement machineTypeSelect;
    @FindBy(xpath = "//md-option[contains(., 'n1-standard-8')]")
    private WebElement machineTypeOption;
    @FindBy(xpath = "//md-checkbox[contains(.,'Add GPUs.')]")
    private WebElement addGPUCheckBox;
    @FindBy(xpath = "//label[text()='GPU type']/following-sibling::md-select")
    private WebElement gpuTypeSelect;
    @FindBy(xpath = "//md-option[contains(., 'NVIDIA Tesla V100')]")
    private WebElement nvidiaTesla;
    @FindBy(xpath = "//label[text()='Number of GPUs']/following-sibling::md-select")
    private WebElement numberOfGPUSelect;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[@value='1']")
    private WebElement numberOfGPUOption;
    @FindBy(xpath = "//label[text()='Local SSD']/following-sibling::md-select")
    private WebElement localSSDSelect;
    @FindBy(xpath = "//md-option[contains(., '2x375')]")
    private WebElement localSSDOption;
    @FindBy(xpath = "//label[text()='Datacenter location']/following-sibling::md-select")
    private WebElement dataCenterSelect;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[contains(., 'Frankfurt')]")
    private WebElement frankfurtDataCenter;
    @FindBy(xpath = "//label[text()='Committed usage']/following-sibling::md-select")
    private WebElement committedUsageSelect;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[contains(., '1 Year')]")
    private WebElement committedUsageOption;
    @FindBy(xpath = "//button[contains(text(),'Add to Estimate')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//b[contains(text(), 'Total Estimated Cost:')]")
    private WebElement estimatedCost;
    @FindBy(id = "Email Estimate")
    private WebElement emailEstimateButton;
    @FindBy(xpath = "//label[text()='Email ']/following-sibling::input")
    private WebElement emailInput;
    @FindBy(xpath = "//button[contains(text(),'Send Email')]")
    private WebElement sendEmailButton;
    public GCPricingCalculatorPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains("calculator-legacy")) {
            driver.get("https://cloud.google.com/products/calculator-legacy");
        }
        setUp(driver);
        PageFactory.initElements(driver, this);
    }

    public GCPricingCalculatorPage updateDriver(WebDriver driver) {
        setUp(driver);
        return this;
    }

    private void setUp(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        this.driver.switchTo().frame(0);
        this.driver.switchTo().frame(0);
    }

    public GCPricingCalculatorPage chooseComputeEngine() {
        computeEngineTab.click();
        return this;
    }

    public GCPricingCalculatorPage fillForm() {
        enterNumberOfInstances("4");
        selectOperatingSystem(waitVisibility(freeOperatingSystem));
        selectProvisioningModel(waitVisibility(regularModel));
        selectMachineFamily(waitVisibility(generalFamily));
        selectSeries(waitVisibility(n1Series));
        selectMachineType(waitVisibility(machineTypeOption));
        clickAddGPUCheckBox();
        selectGPUType(waitVisibility(nvidiaTesla));
        selectNumberOfGPU(waitVisibility(numberOfGPUOption));
        selectLocalSSD(waitVisibility(localSSDOption));
        selectDataCenter(waitVisibility(frankfurtDataCenter));
        selectCommittedUsage(waitVisibility(committedUsageOption));
        return this;
    }

    public void selectDataCenter(WebElement dataCenterOption) {
        dataCenterSelect.click();
        dataCenterOption.click();
    }

    public void selectLocalSSD(WebElement localSSDOption) {
        localSSDSelect.click();
        localSSDOption.click();
    }

    public void selectNumberOfGPU(WebElement numberOfGPUOption) {
        numberOfGPUSelect.click();
        numberOfGPUOption.click();
    }

    public void selectGPUType(WebElement gpuTypeOption) {
        gpuTypeSelect.click();
        gpuTypeOption.click();
    }

    public void clickAddGPUCheckBox() {
        addGPUCheckBox.click();
    }

    public void selectMachineType(WebElement machineTypeOption) {
        machineTypeSelect.click();
        machineTypeOption.click();
    }

    public void selectSeries(WebElement seriesOption) {
        seriesSelect.click();
        seriesOption.click();
    }

    public void selectCommittedUsage(WebElement committedUsageOption) {
        committedUsageSelect.click();
         committedUsageOption.click();
    }

    public void selectMachineFamily(WebElement machineFamilyOption) {
        machineFamilySelect.click();
        machineFamilyOption.click();
    }

    public void selectProvisioningModel(WebElement provisioningModelOption) {
        provisioningModelSelect.click();
        provisioningModelOption.click();
    }

    public WebElement waitVisibility(WebElement webElement) {
        return wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public void selectOperatingSystem(WebElement operatingSystemOption) {
        operatingSystemSelect.click();
        operatingSystemOption.click();
    }

    public void enterNumberOfInstances(String number) {
        numberOfInstancesInput.sendKeys(number);
    }

    public GCPricingCalculatorPage addToEstimate() {
        wait.until(ExpectedConditions.visibilityOf(addToEstimateButton)).click();
        return this;
    }

    public GCPricingCalculatorPage emailEstimate() {
        wait.until(ExpectedConditions.elementToBeClickable(emailEstimateButton)).click();
        return this;
    }

    public GCPricingCalculatorPage fillEmail() {
        emailInput.sendKeys(Keys.CONTROL + "v");
        return this;
    }

    public GCPricingCalculatorPage sendEmail() {
        wait.until(ExpectedConditions.elementToBeClickable(sendEmailButton)).click();
        return this;
    }

    public boolean isEstimatedCostVisible() {
        String totalEstimatedCost = wait.until(ExpectedConditions.visibilityOf(estimatedCost)).getText();
        return totalEstimatedCost.contains("Total Estimated Cost: USD");
    }

    public String getTotalEstimatedCost() {
        return wait.until(ExpectedConditions.visibilityOf(estimatedCost)).getText();
    }
}

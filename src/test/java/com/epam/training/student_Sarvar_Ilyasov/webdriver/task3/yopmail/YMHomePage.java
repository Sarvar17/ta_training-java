package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.yopmail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YMHomePage {
    private final String HOMEPAGE_URL = "https://yopmail.com/";
    private final String RANDOM_EMAIL_URL = "https://yopmail.com/email-generator";
    private final WebDriver driver;
    @FindBy(className = "txtlien")
    private WebElement randomEmailGeneratorButton;
    public YMHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    public YMHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        return this;
    }

    public YMEmailGeneratorPage createRandomEmail() {
        randomEmailGeneratorButton.click();
        driver.get(RANDOM_EMAIL_URL);
        return new YMEmailGeneratorPage(driver);
    }
}

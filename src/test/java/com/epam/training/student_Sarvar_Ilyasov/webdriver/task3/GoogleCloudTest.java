package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3;

import com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.google_cloud.GCHomePage;
import com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.google_cloud.GCPricingCalculatorPage;
import com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.utils.CostUtils;
import com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.utils.TabUtils;
import com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.yopmail.YMEmailInboxPage;
import com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.yopmail.YMHomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoogleCloudTest {
    private static WebDriver driver;
    private static GCPricingCalculatorPage googleCloudTab;

    @BeforeAll
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        googleCloudTab = new GCHomePage(driver).openPage()
                .searchForTerms("Google Cloud Platform Pricing Calculator")
                .openCalculator().chooseComputeEngine().fillForm().addToEstimate();
    }

    @AfterAll
    public static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    @Order(1)
    public void testEstimatedCostVisible() {
        Assertions.assertTrue(googleCloudTab.isEstimatedCostVisible(),
                "Estimated Cost in GC Pricing Calculator is not visible.");
    }

    @Test
    @Order(2)
    public void testEstimatedCostInInbox() {
        String totalEstimatedCost = CostUtils.extractCost(googleCloudTab.getTotalEstimatedCost());
        googleCloudTab.emailEstimate();
        TabUtils.newTab(driver);
        YMEmailInboxPage yopmailTab = new YMHomePage(driver).openPage()
                .createRandomEmail().copyEmail().checkEmailInbox();
        TabUtils.switchToGoogle(driver);
        googleCloudTab.updateDriver(driver).fillEmail().sendEmail();
        TabUtils.switchToYopmail(driver);
        Assertions.assertTrue(yopmailTab.updateDriver(driver)
                        .getEstimatedCost(totalEstimatedCost).contains(totalEstimatedCost),
                "Estimated Cost which is sent by GC Pricing Calculator does not match.");
    }
}

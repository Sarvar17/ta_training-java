package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.yopmail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class YMEmailGeneratorPage {
    private final WebDriver driver;
    private final WebDriverWait wait;
    @FindBy(id = "cprnd")
    private WebElement copyEmailButton;
    @FindBy(xpath = "//span[contains(text(),'Check Inbox')]")
    private WebElement checkEmailInboxButton;
    public YMEmailGeneratorPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public YMEmailGeneratorPage copyEmail() {
        wait.until(ExpectedConditions.visibilityOf(copyEmailButton)).click();
        return this;
    }

    public YMEmailInboxPage checkEmailInbox() {
        wait.until(ExpectedConditions.visibilityOf(checkEmailInboxButton)).click();
        return new YMEmailInboxPage(driver);
    }
}

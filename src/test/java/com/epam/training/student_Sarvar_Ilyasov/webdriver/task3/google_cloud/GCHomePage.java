package com.epam.training.student_Sarvar_Ilyasov.webdriver.task3.google_cloud;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GCHomePage {
    private final String HOMEPAGE_URL = "https://cloud.google.com/";
    private final WebDriver driver;
    @FindBy(className = "YSM5S")
    private WebElement searchIcon;
    @FindBy(name = "q")
    private WebElement searchTextBox;
    public GCHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public GCHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        return this;
    }

    public GCSearchResultsPage searchForTerms(String term) {
        searchIcon.click();
        searchTextBox.sendKeys(term);
        searchTextBox.sendKeys(Keys.ENTER);

        return new GCSearchResultsPage(driver);
    }
}
